local fn = vim.fn
local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
if fn.empty(fn.glob(install_path)) > 0 then
  packer_bootstrap = fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
end

vim.cmd [[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerCompile
  augroup end
]]

vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function()
    use 'wbthomason/packer.nvim'

    -- Functionality
    use 'scrooloose/syntastic'
    use 'scrooloose/nerdcommenter'
	
    use {
		'nvim-treesitter/nvim-treesitter',
		run = ':TSUpdate',
		config = function()
			require('nvim-treesitter.configs').setup {
				ensure_installed = { "norg", "javascript", "python" },
				highlight = {
					enable = true,
				}
			}
		end,
	}

    use 'andweeb/presence.nvim'

    -- Languages
    use 'ziglang/zig.vim'
    use 'bakpakin/fennel.vim'
	use {
		"nvim-neorg/neorg",
		config = function()
			require('neorg').setup {
			    load = {
                    ["core.defaults"] = {},
                    ["core.concealer"] = {},
                    ["core.dirman"] = {
                        config = {
                            workspaces = {
                                notes = "~/notes",
                            }
                        }
                    },
                }
            }
		end,
        run = ":Neorg sync-parsers",
		requires = "nvim-lua/plenary.nvim",
	}
    
    -- Color schemes
    use 'morhetz/gruvbox'
    use 'sickill/vim-monokai'
    use 'sainnhe/sonokai'

	if packer_bootstrap then
		require('packer').sync()
	end
end)
