#!/usr/bin/zsh
script_dir=~/.cm/scripts
selected_script=$(ls ${script_dir} | dmenu -i -fn "FiraCode")
if [ $? -eq 0 ]; then
    exec "${script_dir}/${selected_script}"
fi
